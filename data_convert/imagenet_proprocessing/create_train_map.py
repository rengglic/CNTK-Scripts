import os

id_file = "CAT_IDS.txt";
folders = ["/scratch/snx3000/rengglic/data_cntk/train"]
train_map = "train_map.txt";

ids=dict();
with open(id_file) as f:
    for line in f:
        vals = line.split();
        ids[vals[1].strip()] = int(vals[0]);

with open(train_map, 'w') as f:
    for folder in folders:
        print("Processing top folder: {0}".format(folder));
        for subfolder in os.listdir(folder):
            print(" Subfolder: {0} -> ID: {1}".format(subfolder, ids[subfolder]));
            for img in os.listdir(folder + "/" + subfolder):
                f.write("{0}/{1}/{2}\t{3}\n".format(folder, subfolder, img, ids[subfolder]));
