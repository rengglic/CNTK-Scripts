import pickle
import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({'font.size':22})

from os import listdir
from os.path import isfile, join

networks = ['AlexNet', 'ResNet50', 'ResNet152', 'BN-Inception', 'VGG19']

# for inputPath in ['AlexNet']:

inputPath = './data/'
outputPath = './plots/'
outputFileName = "time"

gpus=[2]
quants = []

# quantizer=['1bit', '32bit', '2bit','4bit']
# quantizer_output = ['1bitSGD', '32bit', '2bit ($d$=128)', '4bit ($d$=1024)']
# with1bit = "_with1bit"

epoch_time_dict = dict()
agg_time_dict = dict()

for filename in sorted([f for f in listdir(inputPath) if isfile(join(inputPath, f))]):
    print('Working on file ', filename)

    # quant = filename[:filename.find('_')]
    # num_gpus = (int)(filename[filename.find('_') + 1:filename.find('_', filename.find('_') + 1)])
    quant = filename

    curr_times = []
    aggregation_time = []
    minibatch_size = 0
    numMBsToShowResult = 0
    syncPerfStats = 0

    with open(inputPath + "/" + filename) as f:
        for line in f:
            if (line.find("minibatchSize") != -1):
                if(line[line.find("=")+2] == "$"):
                    continue
                minibatch_size = (int)(line[line.find("=")+2:-1])

            if (line.find("epochTime=") != -1):
                if(line[line.find("=")+2] == "$"):
                    continue
                curr_times.append((float)(line[line.find("epochTime=")+10:-2]))
                # print(curr_times[-1])

            if (line.find("numMBsToShowResult") != -1):
                numMBsToShowResult = (int)(line[line.find("=")+1:-1])

            if (line.find("syncPerfStats") != -1):
                syncPerfStats = (int)(line[line.find("=")+1:-1])

            if (line.find("aggregation time:") != -1):
                aggregation_time.append((float)(line[line.find("aggregation time:")+17:line.find("aggregation time:")+24]))

            # if (line.find("numElementsPerBucket =") != -1 and len(line) < 50):
            #     if (quant == "2bit"):
            #         print(line)

    total_time = 0
    total_agg_time = 0

    for i in range(len(curr_times)):
        total_time += curr_times[i]

    start_id = 0
    for i in range(start_id, len(aggregation_time)):
        #if (i % 2 == 1):
        total_agg_time += aggregation_time[i]

    #divide = (len(aggregation_time) - start_id) / 2
    divide = (len(aggregation_time) - start_id)

    #print(total_time)
    print(total_agg_time / divide)

    epoch_time_dict[quant] = total_time
    #agg_time_dict[quant] = 1281167 / minibatch_size * total_agg_time / divide
    agg_time_dict[quant] = total_agg_time

    quants.append(quant)


width=0.2
cols=[[0,0.4470,0.7410],[0.8500,0.3250,0.0980],[0.9290,0.6940,0.1250],[0.5,0.95,0.5],[0,0.4470,0.7410],[0.8500,0.3250,0.0980],[0.9290,0.6940,0.1250],[0,0.4470,0.7410],[0.8500,0.3250,0.0980]]

plt.figure(figsize=(8.4,5))
for i,quant in enumerate(quants):
    lab=quant
    X = np.arange(len(gpus))

    agg_time = [agg_time_dict[quant]]
    comp_time = [epoch_time_dict[quant] - agg_time_dict[quant]]

    plt.bar(X +width*i, agg_time, width, color=cols[i], align="center")
    plt.bar(X +width*i, comp_time, width, color=cols[i], label=lab, alpha = 0.7, bottom=agg_time, align="center")
    #plt.bar(X +width*i, comp_time, width, color=cols[i], label=lab, alpha = 0.7, align="center")
plt.legend(loc='upper center', ncol=len(gpus), fontsize=14, bbox_to_anchor=(0.8, 1))
plt.grid(True)
#plt.xticks([-0.7, 0.30, 1.30, 2.30, 3.30], [], fontsize=15)
plt.xticks([-0.7, 0.30, 1.30, 2.30, 3.30], [], fontsize=15)
# plt.xticks([0.3, 1.3, 2.3], gpus, fontsize=15)
#for i,q in enumerate(quantizer):
#    plt.text(i+0.2,-1.5,labs[q],fontsize=16)
#plt.ylabel('Time for epoch (seconds)', fontsize=18)
plt.ylabel('Time for 200 MB (seconds)', fontsize=18)
#plt.xlim([-1.8,2.3])

low = min(epoch_time_dict[quant] for quant in quants)
high = max(epoch_time_dict[quant] for quant in quants)
plt.ylim([0, high + 0.9*(high-low)])

plt.title("VGG19 - ImageNet1K")
plt.tight_layout()
#plt.savefig(outputPath + outputFileName + ".png")
plt.savefig(outputPath + outputFileName + ".pdf")
